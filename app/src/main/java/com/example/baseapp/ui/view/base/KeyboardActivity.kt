package com.example.baseapp.ui.view.base

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.databinding.ViewDataBinding

/**
 * TextField를 사용하는 화면의 경우 해당 Activity를 상속받아야함
 * hideKeyboardWhenClickOutside() 함수도 꼭 선언해야됨
 */
abstract class KeyboardActivity<T: ViewDataBinding>: BaseActivity<T>() {

    protected fun hideKeyboard() {
        val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = this.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(this)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    /* 텍스트뷰 이외의 모든 뷰가 터치되면 이벤트 발생 */
    protected fun hideKeyboardWhenClickOutside(view: View) {
        // 텍스트뷰 이외의 모든 뷰가 터치되면 이벤트 등록
        if (view !is EditText) {
            view.setOnTouchListener { v, event ->
                hideKeyboard()
                this.currentFocus?.clearFocus()
                false
            }
        }
        //If a layout container, iterate over children and seed recursion.
        if (view is ViewGroup) {
            for (i in 0 until view.childCount) {
                val innerView = view.getChildAt(i)
                hideKeyboardWhenClickOutside(innerView)
            }
        }
    }
}
