package com.example.baseapp.ui.view.login

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import com.example.baseapp.R
import com.example.baseapp.databinding.ActivityLoginBinding
import com.example.baseapp.ui.view.base.KeyboardActivity
import com.example.baseapp.ui.dialog.PopupDialog
import com.example.baseapp.ui.view.main.MainActivity

class LoginActivity : KeyboardActivity<ActivityLoginBinding>(), LoginViewModel.LoginListener {
    override val layoutResourceID: Int
        get() = R.layout.activity_login

    private val viewModel: LoginViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    private fun initView() {
        binding.viewModel = viewModel
        viewModel.listener = this

        viewModel.userId.observe(this) {
            viewModel.isChecked.value = it.isNotEmpty()
            viewModel.observedUserId.value = it
        }

        viewModel.userPassword.observe(this) {
            viewModel.notObservedUserPassword = it
        }

        hideKeyboardWhenClickOutside(binding.root)
    }

    /* LoginListener Delegate */
    override fun showLoadingBar() {
        showLoadingDialog()
    }

    override fun hideLoadingBar() {
        hideLoadingDialog()
    }

    override fun showDialog(message: String?) {
        message?.let { PopupDialog(this, it).show() } ?: PopupDialog(
            this,
            getString(R.string.network_error)
        ).show()
    }

    override fun goToMain() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
    /* */
}
