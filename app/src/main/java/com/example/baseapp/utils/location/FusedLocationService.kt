package com.example.baseapp.utils.location

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.os.Looper
import androidx.core.app.NotificationCompat
import androidx.lifecycle.MutableLiveData
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.baseapp.R
import com.example.baseapp.ui.view.main.gps.ForegroundLocationServiceActivity
import com.example.baseapp.utils.BaseUtil
import com.example.baseapp.utils.Constant.commonLocation
import com.example.baseapp.utils.LogType
import com.google.android.gms.location.*
import java.util.concurrent.TimeUnit

class FusedLocationService : Service() {
    internal val action = "FusedLocationService"
    private val notificationId = 12345678
    private val notificationChannelId = "baseapp_channel"

    companion object {
        var isRunning = MutableLiveData(false)
    }

    private lateinit var notificationManager: NotificationManager
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var locationRequest: LocationRequest

    override fun onCreate() {
        BaseUtil().log(LogType.D, "onCreate")
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        locationRequest = LocationRequest.create().apply {
            interval = TimeUnit.SECONDS.toMillis(2)
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        BaseUtil().log(LogType.D, "onStartCommand")

        when (intent.action) {
            "START" -> {
                startForegroundService()
            }
            "STOP" -> {
                stopForegroundService()
            }
        }
        return START_STICKY
    }

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onDestroy() {
        BaseUtil().log(LogType.D, "onDestroy")
    }

    private fun startForegroundService() {
        isRunning.value = true

        val notification = generateNotification("ready")
        startForeground(notificationId, notification)

        try {
            fusedLocationProviderClient.requestLocationUpdates(
                locationRequest, locationCallback, Looper.getMainLooper()
            )
        } catch (e: SecurityException) {
            stopForegroundService()
        }
    }

    private fun stopForegroundService() {
        isRunning.value = false

        stopForeground(true)
        stopSelf()

        try {
            val removeTask = fusedLocationProviderClient.removeLocationUpdates(locationCallback)
            removeTask.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    stopSelf()
                } else {
                }
            }
        } catch (e: SecurityException) {
        }
    }

    private fun generateNotification(location: String): Notification {
        val title = getString(R.string.app_name)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(
                notificationChannelId, title, NotificationManager.IMPORTANCE_DEFAULT
            )

            notificationManager.createNotificationChannel(notificationChannel)
        }

        val bigTextStyle =
            NotificationCompat.BigTextStyle().setBigContentTitle(title).bigText(location)

        val launchActivityIntent = Intent(this, ForegroundLocationServiceActivity::class.java)
        val activityPendingIntent = PendingIntent.getActivity(
            this, 0, launchActivityIntent, 0
        )

        val notificationCompatBuilder =
            NotificationCompat.Builder(applicationContext, notificationChannelId)

        return notificationCompatBuilder.setStyle(bigTextStyle).setContentTitle(title)
            .setContentText(location).setSmallIcon(R.mipmap.ic_launcher)
            .setDefaults(NotificationCompat.DEFAULT_ALL).setOngoing(true)
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            .setContentIntent(activityPendingIntent).build()
    }

    private var locationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            super.onLocationResult(locationResult)

            val position = "${locationResult.lastLocation.latitude}, ${locationResult.lastLocation.longitude}"
            BaseUtil().log(LogType.D, "locationCallback", position)

            val intent = Intent(action)
            intent.putExtra(commonLocation, locationResult.lastLocation)
            LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)

            notificationManager.notify(
                notificationId, generateNotification(position)
            )
        }
    }
}
