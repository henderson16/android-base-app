package com.example.baseapp.utils

import android.content.Context
import android.content.Context.VIBRATOR_SERVICE
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.MediaPlayer
import android.media.RingtoneManager
import android.os.*
import android.util.Log
import com.example.baseapp.BuildConfig.DEBUG


enum class LogType {
    D, E
}

class BaseUtil {
    /**
     * 숫자에 컴마 넣는 유틸 함수
     * @param number
     */
    fun addComma(num: Int): String {
        if (num >= 0) {
            var number = num.toString()
            var i = number.length - 3
            while (i > 0) {
                number = number.substring(0, i) + "," + number.substring(i)
                i -= 3
            }
            return number
        } else {
            var number = (num * -1).toString()
            var i = number.length - 3
            while (i > 0) {
                number = number.substring(0, i) + "," + number.substring(i)
                i -= 3
            }
            //TODO change something
            return ("-$number")
        }
    }

    fun playSound(context: Context) {
        //시스템 소리가 무음/진동일 때도 동작하도록 미디어 소리로 설정 및 볼륨 설정
        val audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        val maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC)
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, (maxVolume * 0.3).toInt(), 0)

        val ringtone = MediaPlayer()
        val uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        ringtone.setDataSource(context, uri)
        ringtone.prepare()
        ringtone.start()
    }

    fun playVibration(context: Context) {
        val vibrator = context.getSystemService(VIBRATOR_SERVICE) as Vibrator

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val vibrationEffect =
                VibrationEffect.createOneShot(1000, VibrationEffect.DEFAULT_AMPLITUDE)
            //백그라운드에서도 진동이 울리도록 attribute 설정
            val audioAttributes =
                AudioAttributes.Builder().setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_ALARM).build()
            vibrator.vibrate(vibrationEffect, audioAttributes)
        } else {
            //deprecated in API 26
            vibrator.vibrate(2000)
        }
    }

    fun log(type: LogType, method: String) {
        log(type, method, " ")
    }

    fun log(type: LogType, method: String, message: String) {
        if (DEBUG) {
            when (type) {
                LogType.D -> {
                    Log.d("!@# $method", message)
                }
                LogType.E -> {
                    Log.e("!@# $method", message)
                }

            }
        }
    }
}