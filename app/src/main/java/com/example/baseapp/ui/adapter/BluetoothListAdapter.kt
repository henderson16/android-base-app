package com.example.baseapp.ui.adapter

import android.bluetooth.BluetoothDevice
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.baseapp.R
import com.example.baseapp.databinding.ItemBleBinding


class BluetoothListAdapter(private val context: Context) :
    RecyclerView.Adapter<BluetoothListAdapter.ViewHolder>() {
    private var list = ArrayList<BluetoothDevice>()
    var listener: Listener? = null

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_ble, viewGroup, false)
        return ViewHolder(ItemBleBinding.bind(view))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.title.text =
            "name : ${list[position].name}\naddress : ${list[position].address}"
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun addItem(data: BluetoothDevice) {
        list.add(data)
        notifyItemInserted(list.size)
    }

    fun clearItem() {
        list.clear()
        notifyDataSetChanged()
    }

    inner class ViewHolder(var binding: ItemBleBinding) : RecyclerView.ViewHolder(binding.root) {
        init {
            binding.root.setOnClickListener {
                listener?.clickItem(list[adapterPosition])
            }
        }
    }

    interface Listener {
        fun clickItem(device: BluetoothDevice)
    }
}