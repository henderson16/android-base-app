package com.example.baseapp.ui.view.main.ble

import android.Manifest
import android.bluetooth.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.baseapp.R
import com.example.baseapp.databinding.ActivityBluetoothScanBinding
import com.example.baseapp.ui.adapter.BaseAdapter
import com.example.baseapp.ui.adapter.BluetoothListAdapter
import com.example.baseapp.ui.view.base.BaseActivity
import com.example.baseapp.utils.BaseUtil
import com.example.baseapp.utils.Constant.BLUETOOTH_UUID
import com.example.baseapp.utils.LogType
import java.io.IOException
import java.io.OutputStream

class BluetoothScanActivity : BaseActivity<ActivityBluetoothScanBinding>(), BluetoothListAdapter.Listener, AcceptThread.Listener,
    ConnectThread.Listener, ConnectedThread.Listener {
    override val layoutResourceID: Int
        get() = R.layout.activity_bluetooth_scan

    private var bondedListAdapter: BluetoothListAdapter? = null
    private var scannedListAdapter: BluetoothListAdapter? = null
    private var chatListAdapter: BaseAdapter? = null
    private var bluetoothAdapter: BluetoothAdapter? = null

    private val connectedThread = ArrayList<ConnectedThread>()
    private val discoverableDuration = 20

    private val requiredPermissions = arrayOf(
        Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION
    )

    private val requiredPermissionsOver11 = arrayOf(
        Manifest.permission.BLUETOOTH_SCAN, Manifest.permission.BLUETOOTH_CONNECT
    )

    private val activityResult = registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { isGranted ->
        if (java.lang.Boolean.TRUE == isGranted[Manifest.permission.ACCESS_FINE_LOCATION] && java.lang.Boolean.TRUE == isGranted[Manifest.permission.ACCESS_COARSE_LOCATION]) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.S) {
                bluetoothAdapter?.let {
                    startScan(it)
                }
            } else {
                activityResultOver11.launch(requiredPermissionsOver11)
            }
        } else {
            Toast.makeText(this, R.string.no_gps_permission, Toast.LENGTH_SHORT).show()
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            val uri = Uri.fromParts("package", packageName, null)
            intent.data = uri
            startActivity(intent)
        }
    }

    private val activityResultOver11 = registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { isGranted ->
        if (java.lang.Boolean.TRUE == isGranted[Manifest.permission.BLUETOOTH_SCAN] && java.lang.Boolean.TRUE == isGranted[Manifest.permission.BLUETOOTH_CONNECT]) {
            bluetoothAdapter?.let {
                startScan(it)
            }
        } else {
            Toast.makeText(this, R.string.no_gps_permission, Toast.LENGTH_SHORT).show()
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            val uri = Uri.fromParts("package", packageName, null)
            intent.data = uri
            startActivity(intent)
        }
    }

    private var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == discoverableDuration) {
            AcceptThread(bluetoothAdapter!!).run {
                this.listener = this@BluetoothScanActivity
                start()
            }
            Toast.makeText(this, R.string.toast_success, Toast.LENGTH_SHORT).show()
        } else if (result.resultCode == RESULT_CANCELED) {
            Toast.makeText(this, R.string.toast_fail, Toast.LENGTH_SHORT).show()
        }
    }

    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            intent.action?.let {
                when (it) {
                    BluetoothDevice.ACTION_FOUND -> {
                        val device = if (Build.VERSION.SDK_INT >= 33) {
                            intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE, BluetoothDevice::class.java)
                        } else {
                            intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE)
                        }
                        scannedListAdapter?.addItem(device!!)
                    }
                    else -> {}
                }
            }

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        init()
        initView()
    }

    override fun onDestroy() {
        super.onDestroy()

        unregisterReceiver(receiver)
    }

    private fun init() {
        val bluetoothManager = getSystemService(BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothAdapter = bluetoothManager.adapter

        registerReceiver(receiver, IntentFilter(BluetoothDevice.ACTION_FOUND))
    }

    private fun initView() {
        binding.activity = this

        bondedListAdapter = BluetoothListAdapter(this)
        bondedListAdapter?.listener = this
        binding.bondedList.adapter = bondedListAdapter
        binding.bondedList.addItemDecoration(DividerItemDecoration(this, LinearLayout.VERTICAL))

        val pairedDevices: Set<BluetoothDevice>? = bluetoothAdapter?.bondedDevices
        pairedDevices?.forEach { device ->
            bondedListAdapter?.addItem(device)
        }

        scannedListAdapter = BluetoothListAdapter(this)
        scannedListAdapter?.listener = this
        binding.scanList.adapter = scannedListAdapter
        binding.scanList.addItemDecoration(DividerItemDecoration(this, LinearLayout.VERTICAL))

        initChatView()
    }

    private fun initChatView() {
        chatListAdapter = BaseAdapter(this)
        binding.chatList.adapter = chatListAdapter
        binding.chatList.addItemDecoration(DividerItemDecoration(this, LinearLayout.VERTICAL))
    }

    private fun startScan(adapter: BluetoothAdapter) {
        scannedListAdapter?.clearItem()
        adapter.startDiscovery()
    }

    fun clickScanButton(view: View) {
        val hasBle = packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH)
        if (hasBle) {
            val bluetoothManager = getSystemService(BLUETOOTH_SERVICE) as BluetoothManager
            if (bluetoothManager.adapter.isEnabled) {
                activityResult.launch(requiredPermissions)
            } else {
                Toast.makeText(this, R.string.off_ble, Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(this, R.string.no_ble, Toast.LENGTH_SHORT).show()
        }
    }

    fun clickDiscoverableButton(view: View) {
        val intent = Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE).apply {
            putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, discoverableDuration)
        }

        resultLauncher.launch(intent)
    }

    fun clickSend(view: View) {
        for (thread in connectedThread) {
            val message = binding.chatEditText.text.toString()
            thread.write(message)
            chatListAdapter?.addItem("me: $message")
            binding.chatEditText.text.clear()
        }
    }

    /* BleDeviceAdapter.Listener */
    override fun clickItem(device: BluetoothDevice) {
        ConnectThread(device, bluetoothAdapter!!).run {
            this.listener = this@BluetoothScanActivity
            start()
        }
    }

    /* ConnectThread Listener, AcceptThread Listener */
    override fun connectionSuccess(socket: BluetoothSocket) {
        runOnUiThread {
            Toast.makeText(this, R.string.toast_bluetooth_connection_success, Toast.LENGTH_SHORT).show()
            binding.chatView.visibility = View.VISIBLE
        }
        ConnectedThread(socket).apply {
            listener = this@BluetoothScanActivity
            connectedThread.add(this)
        }.run {
            start()
        }
    }

    /* ConnectedThread */
    override fun readMessage(message: String) {
        runOnUiThread {
            chatListAdapter?.addItem("friends: $message")
        }
    }
}

class AcceptThread(val adapter: BluetoothAdapter) : Thread() {

    var listener: Listener? = null

    private val serverSocket: BluetoothServerSocket? by lazy(LazyThreadSafetyMode.NONE) {
        adapter.listenUsingInsecureRfcommWithServiceRecord("", BLUETOOTH_UUID)
    }

    override fun run() {
        var shouldLoop = true
        while (shouldLoop) {
            val socket: BluetoothSocket? = try {
                serverSocket?.accept()
            } catch (e: Exception) {
                BaseUtil().log(LogType.D, "error $e")
                shouldLoop = false
                null
            }
            socket?.also {
                listener?.connectionSuccess(it)
                cancel()
            }
        }
    }

    fun cancel() {
        try {
            serverSocket?.close()
        } catch (e: IOException) {
            BaseUtil().log(LogType.D, "error $e")
        }
    }

    interface Listener {
        fun connectionSuccess(socket: BluetoothSocket)
    }
}

class ConnectThread(device: BluetoothDevice, val adapter: BluetoothAdapter) : Thread() {

    var listener: Listener? = null

    private val socket: BluetoothSocket? by lazy(LazyThreadSafetyMode.NONE) {
        device.createRfcommSocketToServiceRecord(BLUETOOTH_UUID)
    }

    override fun run() {
        adapter.cancelDiscovery()
        try {
            socket?.connect()
            socket?.let {
                listener?.connectionSuccess(it)
            }
        } catch (e: Exception) {
            cancel()
            BaseUtil().log(LogType.D, "error $e")
        }
    }

    fun cancel() {
        try {
            socket?.close()
        } catch (e: Exception) {
            BaseUtil().log(LogType.D, "error $e")
        }
    }

    interface Listener {
        fun connectionSuccess(socket: BluetoothSocket)
    }
}

class ConnectedThread(private val socket: BluetoothSocket) : Thread() {

    private val inputStream = socket.inputStream
    private val outputStream: OutputStream = socket.outputStream
    private val buffer: ByteArray = ByteArray(1024)

    var listener: Listener? = null

    override fun run() {
        try {
            while (true) {
                val bytes = inputStream.read(buffer)
                val inputBuffer = String(buffer, 0, bytes)
                listener?.readMessage(inputBuffer)
                BaseUtil().log(LogType.D, "reaad : $inputBuffer")
            }
        } catch (e: Exception) {
            cancel()
            BaseUtil().log(LogType.D, "error $e")
        }
    }

    fun write(message: String) {
        outputStream.write(message.toByteArray())
        BaseUtil().log(LogType.D, "write : $message")
    }

    fun cancel() {
        try {
            socket.close()
        } catch (e: Exception) {
            BaseUtil().log(LogType.D, "error $e")
        }
    }

    interface Listener {
        fun readMessage(message: String)
    }
}