package com.example.baseapp.utils.location;//package com.example.baseapp.utils;
//
//import android.Manifest;
//import android.app.Notification;
//import android.app.NotificationChannel;
//import android.app.NotificationManager;
//import android.app.PendingIntent;
//import android.app.Service;
//import android.content.Context;
//import android.content.Intent;
//import android.content.pm.PackageManager;
//import android.location.Location;
//import android.os.Build;
//import android.os.IBinder;
//import android.os.Looper;
//
//import androidx.annotation.Nullable;
//import androidx.core.app.ActivityCompat;
//import androidx.core.app.NotificationBuilderWithBuilderAccessor;
//import androidx.core.app.NotificationCompat;
//import androidx.lifecycle.MutableLiveData;
//import androidx.localbroadcastmanager.content.LocalBroadcastManager;
//
//import com.example.baseapp.R;
//import com.example.baseapp.ui.view.main.gps.ForegroundServiceGpsActivity;
//import com.google.android.gms.location.FusedLocationProviderClient;
//import com.google.android.gms.location.LocationCallback;
//import com.google.android.gms.location.LocationRequest;
//import com.google.android.gms.location.LocationResult;
//import com.google.android.gms.location.LocationServices;
//import com.google.android.gms.tasks.Task;
//
//import java.util.concurrent.TimeUnit;
//
//public class GpsService extends Service {
//
//    public String locationBroadcast = "baseapp_LOCATION_BROADCAST";
//    public String locationCallBackName = "baseapp.extra.LOCATION";
//    public int notificationId = 12345678;
//    public String notificationChannelId = "baseapp_channel";
//
//    public static MutableLiveData<Boolean> isRunning = new MutableLiveData<>();//false
//
//    public NotificationManager notificationManager;
//    public FusedLocationProviderClient fusedLocationProviderClient;
//    public LocationRequest locationRequest;
//    public LocationCallback locationCallback;
//
//    private Location currentLocation;
//
//    @Override
//    public void onCreate() {
//        //super.onCreate();
//        locationRequest = new LocationRequest();
//        locationCallback = new LocationCallback();
//
//        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
//
//        //apply는 코틀린에서 변수 선언과 동시에 초기화하는 스코프 펑션인데 객체 초기화 하고 객체.~~ = xx 이런식으로 객체 프로퍼티를 하나하나 세터를 이용해서 셋해주면 될 듯~!~!
//        locationRequest = new LocationRequest();
//        locationRequest.setInterval(TimeUnit.SECONDS.toMillis(2));
//        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
//
//        locationCallback = new LocationCallback() {
//            @Override
//            public void onLocationResult(LocationResult locationResult) {
//                super.onLocationResult(locationResult);
//
//                currentLocation = locationResult.getLastLocation();
//
//                Intent intent = new Intent(locationBroadcast);
//                intent.putExtra(locationCallBackName, currentLocation);
//                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
//
//                notificationManager.notify(
//                        notificationId, generateNotification(currentLocation)
//                );
//            }
//
//        };
//    }
//
//    @Override
//    public int onStartCommand(Intent intent, int flags, int startId) {
//        //return super.onStartCommand(intent, flags, startId);
//        switch (intent.getAction()){
//            case "START":
//                startForegroundService();
//                break;
//            case "STOP":
//                stopForegroundService();
//                break;
//        }
//        return START_STICKY;
//    }
//
//    @Nullable
//    @Override
//    public IBinder onBind(Intent intent) {
//        return null;
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//    }
//
//    private void startForegroundService() {
//        isRunning.setValue(true);
//
//        Notification notification = generateNotification(currentLocation);
//        startForeground(notificationId, notification);
//        try {
//            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
//                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                return;
//            }
//            fusedLocationProviderClient.requestLocationUpdates(
//                    locationRequest, locationCallback, Looper.getMainLooper()
//            );
//        } catch (SecurityException e) {
//            stopForegroundService();
//        }
//    }
//
//    private void stopForegroundService() {
//        isRunning.setValue(false);
//
//        stopForeground(true);
//        stopSelf();
//
//        try {
//            Task removeTask = fusedLocationProviderClient.removeLocationUpdates(locationCallback);
//            removeTask.addOnCompleteListener(task -> {
//                if (task.isSuccessful()) {
//                    stopSelf();
//                } else {
//                }
//            });
//        } catch (SecurityException e) {
//        }
//    }
//
//
//    private Notification generateNotification(Location location){
//
//        String title = getString(R.string.app_name);
//        String message="no Location";
//        if(location!=null){
//             message = location.toString();
//        }
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            NotificationChannel notificationChannel = new NotificationChannel(
//                    notificationChannelId, title, NotificationManager.IMPORTANCE_DEFAULT
//            );
//
//            notificationManager.createNotificationChannel(notificationChannel);
//        }
//
//        //bigTextStyle =  NotificationCompat.BigTextStyle().setBigContentTitle(title).bigText(message);
//
//        Intent launchActivityIntent = new Intent(this, ForegroundServiceGpsActivity.class);
//        PendingIntent activityPendingIntent = PendingIntent.getActivity(
//                this, 0, launchActivityIntent, 0
//        );
//
//         NotificationCompat.Builder notificationCompatBuilder =
//                new NotificationCompat.Builder(getApplicationContext(), notificationChannelId);
//
//        return notificationCompatBuilder.setContentTitle(title)
//                .setContentText(message).setSmallIcon(R.mipmap.ic_launcher)
//                .setDefaults(NotificationCompat.DEFAULT_ALL).setOngoing(true)
//                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
//                .setContentIntent(activityPendingIntent).build();
//    }
//}
