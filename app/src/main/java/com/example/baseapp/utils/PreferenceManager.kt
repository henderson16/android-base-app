package com.example.baseapp.utils

import android.content.Context
import android.content.SharedPreferences
import com.example.baseapp.BuildConfig.APPLICATION_ID
import org.json.JSONArray
import org.json.JSONException


//TODO 시간날 때 되는지 체크해보기....
class PreferenceManager(private val context: Context) {
    private val name: String = APPLICATION_ID
    private val defaultString = ""
    private val defaultBoolean = false
    private val defaultInt = -1

    var preferences: SharedPreferences = getPreferences(context)
    var editor: SharedPreferences.Editor = preferences.edit()

    /**
     * Example :
     * PreferenceManager(application).run {
     * saveArrayString(Constant.searchList, savedSearchList)
     * commit() }
     */
    fun saveString(key: String, value: String?) {
        editor.putString(key, value)
    }

    fun saveBoolean(key: String, value: Boolean) {
        editor.putBoolean(key, value)
    }

    fun saveInt(key: String, value: Int) {
        editor.putInt(key, value)
    }

    fun saveArrayString(key: String, value: ArrayList<String>) {
        val a = JSONArray()
        for (i in 0 until value.size) {
            a.put(value[i])
        }
        if (value.isNotEmpty()) {
            editor.putString(key, a.toString())
        } else {
            editor.putString(key, null)
        }
    }

    fun commit() {
        editor.commit()
    }

    private fun getPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences(name, Context.MODE_PRIVATE)
    }

    /**
     * Example
     * ArrayList<String> list = PreferenceManager(application).getArrayString(Constant.searchList)
     */
    fun getString(key: String?): String? {
        val prefs = getPreferences(context)
        return prefs.getString(key, defaultString)
    }

    fun getBool(key: String?): Boolean {
        val prefs = getPreferences(context)
        return prefs.getBoolean(key, defaultBoolean)
    }

    fun getInt(key: String?): Int {
        val prefs = getPreferences(context)
        return prefs.getInt(key, defaultInt)
    }

    fun getArrayString(key: String?): ArrayList<String> {
        val result = ArrayList<String>()

        val prefs = getPreferences(context)
        val json = prefs.getString(key, null)

        if (json != null) {
            try {
                val a = JSONArray(json)
                for (i in 0 until a.length()) {
                    val data = a.optString(i)
                    result.add(data)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }

        return result
    }
}