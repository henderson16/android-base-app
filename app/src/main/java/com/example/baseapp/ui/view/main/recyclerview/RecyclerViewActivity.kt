package com.example.baseapp.ui.view.web

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import com.example.baseapp.R
import com.example.baseapp.databinding.ActivityRecyclerViewBinding
import com.example.baseapp.ui.adapter.BaseAdapter
import com.example.baseapp.ui.view.base.BaseActivity
import com.example.baseapp.ui.view.main.recyclerview.CenterZoomLayoutManager

class RecyclerViewActivity : BaseActivity<ActivityRecyclerViewBinding>() {
    override val layoutResourceID: Int
        get() = R.layout.activity_recycler_view

    private lateinit var adapter: BaseAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initView()
        setRecyclerView()
    }

    private fun initView() {
        binding.activity = this

        adapter = BaseAdapter(this)
        binding.recyclerView.adapter = adapter
        binding.recyclerView.layoutManager = CenterZoomLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)

        val snap = PagerSnapHelper()
        snap.attachToRecyclerView(binding.recyclerView)
    }

    private fun makeModel(): ArrayList<String> {
        val userList = ArrayList<String>()
        userList.add("11")
        userList.add("22")
        userList.add("33")
        userList.add("44")
        userList.add("55")

        return userList
    }

    private fun setRecyclerView() {
        adapter.setItem(makeModel())
    }
}