package com.example.baseapp.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.baseapp.R

class BaseAdapter(private val context: Context) : RecyclerView.Adapter<BaseAdapter.Cell>() {
    private var list = ArrayList<String>()

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): Cell {
        val view =
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_base, viewGroup, false)
        return Cell(view)
    }

    override fun onBindViewHolder(holder: Cell, position: Int) {
        val data = list[position]

        holder.title.text = data
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setItem(data: ArrayList<String>) {
        list = data
        notifyDataSetChanged()
    }

    fun addItem(data: String) {
        list.add(data)
        notifyItemInserted(list.size)
    }

    inner class Cell internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        //TODO: 데이터바인딩 가능?
        var title = itemView.findViewById<TextView>(R.id.title)!!
    }
}