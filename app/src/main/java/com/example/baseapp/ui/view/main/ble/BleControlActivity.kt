package com.example.baseapp.ui.view.main.ble

import android.bluetooth.le.ScanResult
import android.os.Bundle
import android.view.View
import com.example.baseapp.R
import com.example.baseapp.databinding.ActivityBleControlBinding
import com.example.baseapp.ui.view.base.BaseActivity
import com.example.baseapp.utils.BleUtil

class BleControlActivity : BaseActivity<ActivityBleControlBinding>(), BleUtil.Listener {
    override val layoutResourceID: Int
        get() = R.layout.activity_ble_control

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    private fun initView() {
        binding.activity = this
        BleUtil.listener = this

        val name = BleUtil.getConnectedGatt().device.name
        if (name.isNullOrEmpty()) {
            binding.deviceName.text = "null"
        } else {
            binding.deviceName.text = name
        }
    }

    fun clickSendDataButton(view: View) {
        BleUtil.writeData(BleUtil.CommandType.A)
    }

    fun clickDisconnectButton(view: View) {
        BleUtil.disconnectDevice()
    }

    /* BleUtil.Listener */
    override fun scannedDevice(device: ScanResult) {}

    override fun didConnect() {}

    override fun didDisconnect() {
        onBackPressed()
    }

    override fun readData(command: BleUtil.CommandType, data: String) {
        binding.receivedData.text = data
    }
    /**/
}