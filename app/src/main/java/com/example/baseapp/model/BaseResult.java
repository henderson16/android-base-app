package com.example.baseapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BaseResult {
	/*
	* Common
	*/
	@SerializedName("resultCode")
	@Expose
	private String resultCode;
	@SerializedName("resultMsg")
	@Expose
	private String resultMsg;

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getResultMsg() {
		return resultMsg;
	}

	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}

	/*
	* versionCheck
	*/
	@SerializedName("resultMsgDebug")
	@Expose
	private String resultMsgDebug;
	@SerializedName("downUrl")
	@Expose
	private String downUrl;

	public String getResultMsgDebug() {
		return resultMsgDebug;
	}

	public void setResultMsgDebug(String resultMsgDebug) {
		this.resultMsgDebug = resultMsgDebug;
	}

	public String getDownUrl() {
		return downUrl;
	}

	public void setDownUrl(String downUrl) {
		this.downUrl = downUrl;
	}
}
