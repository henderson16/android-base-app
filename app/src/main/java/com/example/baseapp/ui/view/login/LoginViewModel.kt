package com.example.baseapp.ui.view.login

import android.app.Application
import android.util.Log
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.baseapp.R
import com.example.baseapp.utils.BaseUtil
import com.example.baseapp.utils.LogType
import com.example.baseapp.utils.http.Http
import com.example.baseapp.utils.http.Http_prev
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LoginViewModel(application: Application) : AndroidViewModel(application) {
    var listener: LoginListener? = null

    /*
    * 데이터 바인딩이란 ViewModel에 있는 변수와 View(xml)에 있는 변수를 연결해주는 것.
    * 예로, 사용자가 userId 칸에 값을 입력했으면 별도의 코드없이 ViewModel의 userId에 값이 대입되는 것.
    * (기존에는 Activity에서 getText로 값을 가져온 다음 변수에 값을 대입했음)
    * 안드로이드에서 기본 데이터 바인딩은 View -> Variable
    * 즉 위의 설명처럼 View를 수정하면 자동으로 Variable이 변하지만 반대로 Variable을 수정하면 View가 변하지 않음.
    * 이때 필요한게 two-way 데이터 바인딩.
    * Xml에서 one-way 데이터 바인딩은 @{} 형식으로 Variable을 연결해주는데
    * two-way 데이터 바인딩은 @={} 형식으로 연결.
    * 데이터 바인딩에는 2가지 타입이 있음.
    * 첫번째는 Live Data. 해당 타입은 Observe도 가능하여 값의 변화를 확인하고 이벤트를 발생시킬 수 있음.
    * 두번째는 일반 변수 타입. 해당 타입은 Observe 불가하며 two-way 데이터 바인딩에 사용할 수 없음.
    * (observe할 수 없기 때문에 변수가 변했을 때 이벤트를 못 넘기기 때문이라고 추측)
    *
    * 자세한 사항은 아래 예시 확인
    * */
    var userId = MutableLiveData("")
    var isChecked = MutableLiveData(false)
    var observedUserId = MutableLiveData("")

    var userPassword = MutableLiveData("")
    var notObservedUserPassword = ""

    fun clickLogin(view: View) {
        checkInputData()
    }

    fun clickLogin(view: View, checked: Boolean) {
        checkInputData()
    }

    fun checkInputData() {
        BaseUtil().log(LogType.D, "userId : ${userId.value}")
        BaseUtil().log(LogType.D, "observedUserId : ${observedUserId.value}")
        BaseUtil().log(LogType.D, "userPassword : ${userPassword.value}")
        BaseUtil().log(LogType.D, "notObservedUserPassword : $notObservedUserPassword")

        if (userId.value.isNullOrEmpty()) {
            listener?.showDialog(getApplication<Application>().getString(R.string.id_empty))
        } else {
            callAPI()
        }
    }

    fun callAPI() {
        /* Example
        * val body = HashMap<String, Any>()
        * body["param1"] = "1"
        * body["param2"] = 2
        *  */

        val body = HashMap<String, Any>()
        body["shopCode"] = "testtest1m"

        listener?.showLoadingBar()

        CoroutineScope(Dispatchers.IO).launch {
            val response = Http.httpInterface.getData()
            if (response.isSuccessful) {
                launch(Dispatchers.Main) {
                    listener?.hideLoadingBar()
                    listener?.goToMain()
                }
            }
        }
    }

    interface LoginListener {
        fun showLoadingBar()
        fun hideLoadingBar()
        fun showDialog(message: String?)
        fun goToMain()
    }
}