package com.example.baseapp.utils

import android.bluetooth.*
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.content.Context
import android.os.Handler
import android.os.Looper
import androidx.lifecycle.MutableLiveData
import java.util.*


object BleUtil {

    var listener: Listener? = null

    //Feature For Ble Scan
    private var bluetoothAdapter: BluetoothAdapter? = null
    private const val scanLimitTime = 10000L

    var isScanning = MutableLiveData(false)

    private val leScanCallback: ScanCallback = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult) {
            super.onScanResult(callbackType, result)
            BaseUtil().log(LogType.D, "ScanCallback ${result.device.name}")
            listener?.scannedDevice(result)
        }
    }

    fun scanBle(adapter: BluetoothAdapter) {
        bluetoothAdapter = adapter
        isScanning.value?.let {
            if (it) {
                stopBleScan()
            } else {
                startBleScan()
            }
        }
    }

    private fun startBleScan() {
        BaseUtil().log(LogType.D, "Start Scan")
        isScanning.value = true
        bluetoothAdapter?.bluetoothLeScanner?.startScan(leScanCallback)

        Handler(Looper.getMainLooper()).postDelayed({
            stopBleScan()
        }, scanLimitTime)
    }

    private fun stopBleScan() {
        BaseUtil().log(LogType.D, "Stop Scan")
        isScanning.value = false
        bluetoothAdapter?.bluetoothLeScanner?.stopScan(leScanCallback)
    }

    //Feature For Ble Connection, Read, Write Data
    enum class CommandType {
        A, B, C
    }

    var isConnected = MutableLiveData(false)
    private const val deviceGattUuid = "fec26ec4-6d71-4442-9f81-55bc21d658d0"
    private const val deviceCharacteristicUuid = "fec26ec4-6d71-4442-9f81-55bc21d658d1"
    private const val deviceDescriptorUuid = "00002902-0000-1000-8000-00805f9b34fb"

    private var connectedGatt: BluetoothGatt? = null
    private var connectedChar: BluetoothGattCharacteristic? = null

    fun getConnectedGatt(): BluetoothGatt {
        return connectedGatt!!
    }

    private val gattCallback = object : BluetoothGattCallback() {
        override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {
            when (newState) {
                BluetoothProfile.STATE_CONNECTED -> {
                    BaseUtil().log(LogType.D, "onConnectionStateChange STATE_CONNECTED")
                    isConnected.postValue(true)
                    connectedGatt = gatt
                    gatt.discoverServices()
                    listener?.didConnect()
                }
                BluetoothProfile.STATE_DISCONNECTED -> {
                    BaseUtil().log(LogType.D, "onConnectionStateChange STATE_DISCONNECTED")
                    isConnected.postValue(false)
                    connectedChar = null
                    listener?.didDisconnect()
                }
            }
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
            when (status) {
                BluetoothGatt.GATT_SUCCESS -> {
                    BaseUtil().log(LogType.D, "onServicesDiscovered GATT_SUCCESS")
                    setBleSetting(gatt)
                }
                else -> {
                    BaseUtil().log(LogType.D, "onServicesDiscovered else")
                }
            }
        }

        override fun onCharacteristicChanged(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?) {
            super.onCharacteristicChanged(gatt, characteristic)
            BaseUtil().log(LogType.D, "onCharacteristicChanged ${characteristic?.value}")
            readData(String(characteristic!!.value))
        }

        override fun onDescriptorWrite(gatt: BluetoothGatt?, descriptor: BluetoothGattDescriptor?, status: Int) {
            super.onDescriptorWrite(gatt, descriptor, status)
            /** 필요에 따라 setNotificationDescriptor의 콜백 설정 */
        }
    }

    fun connectToDevice(context: Context, device: BluetoothDevice) {
        device.connectGatt(context, false, gattCallback)
    }

    fun disconnectDevice() {
        connectedGatt?.disconnect()
        connectedGatt?.close()
    }

    fun setBleSetting(gatt: BluetoothGatt) {
        for (service in gatt.services) {
            if (service.uuid.toString() == deviceGattUuid) {
                for (characteristics in service.characteristics) {
                    if (characteristics.uuid.toString() == deviceCharacteristicUuid) {
                        connectedChar = characteristics
                    }
                }
            }
        }
    }

    fun writeData(command: CommandType) {
        BaseUtil().log(LogType.D, "writeData")

        //Temp Data
        val data = byteArrayOf(0x02, 0x54, 0x00, 0x00, 0x00, 0x0D, 0x0A)
        connectedChar?.value = data
        connectedGatt?.writeCharacteristic(connectedChar)
    }

    private fun readData(data: String) {
        BaseUtil().log(LogType.D, "readData")
        listener?.readData(CommandType.A, data)
    }

    /**
     * 필요에 따라 ENABLE_NOTIFICATION_VALUE or DISABLE_NOTIFICATION_VALUE 로 설정
     */
    fun setNotificationDescriptor() {
        connectedGatt?.setCharacteristicNotification(connectedChar, true)

        val descriptor = connectedChar?.getDescriptor(
            UUID.fromString(deviceDescriptorUuid)
        )
        descriptor?.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
        connectedGatt?.writeDescriptor(descriptor)
    }

    interface Listener {
        fun scannedDevice(device: ScanResult)
        fun didConnect()
        fun didDisconnect()
        fun readData(command: CommandType, data: String)
    }
}