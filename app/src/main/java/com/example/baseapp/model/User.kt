package com.example.baseapp.model

data class User(
    val userId: Int,
    val id: Int,
    val title: String,
    val body: String
)
