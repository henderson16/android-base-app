package com.example.baseapp.ui.view.main.foot;

import static androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_DRAGGING;
import static androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_IDLE;
import static androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_SETTLING;

import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.example.baseapp.R;
import com.example.baseapp.databinding.ActivityFooterAnimationBinding;
import com.example.baseapp.ui.adapter.BaseAdapter;
import com.example.baseapp.ui.view.base.BaseActivity;
import com.example.baseapp.ui.view.foot.FooterAnimationViewModel;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;

public class FooterAnimationActivity extends BaseActivity<ActivityFooterAnimationBinding> {
	@Override
	public int getLayoutResourceID() {
		return R.layout.activity_footer_animation;
	}

	private FooterAnimationViewModel viewModel;

	//For Footer
	private final long footerDuration = 200;
	private Handler footerHandler = new Handler();

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		initView();
		initToolBar(binding.toolbar);
	}

	private void initView() {
		viewModel = new ViewModelProvider(this).get(FooterAnimationViewModel.class);
		binding.setViewModel(viewModel);

		BaseAdapter adapter = new BaseAdapter(this);
		binding.recyclerView.setAdapter(adapter);
		ArrayList<String> data = new ArrayList();
		binding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
			@Override
			public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
				super.onScrollStateChanged(recyclerView, newState);
				footerHandler.removeCallbacksAndMessages(null);
				switch (newState) {
					case SCROLL_STATE_IDLE:
						showFooter();
						break;
					case SCROLL_STATE_SETTLING:
						break;
					case SCROLL_STATE_DRAGGING:
						hideFooter();
						break;
				}
			}
		});

		for (int i = 0; i < 100; i++) {
			data.add("1");
		}
		adapter.setItem(data);
	}

	private void showFooter() {
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				binding.footer
						.animate()
						.translationY(0f)
						.setDuration(footerDuration);
			}
		};
		footerHandler.postDelayed(runnable, footerDuration * 2);
	}

	private void hideFooter() {
		binding.footer
				.animate()
				.translationY(binding.footer.getHeight())
				.setDuration(footerDuration);
	}
}
