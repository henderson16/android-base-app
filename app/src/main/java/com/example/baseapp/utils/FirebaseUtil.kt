package com.example.baseapp.utils

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import com.example.baseapp.R
import com.example.baseapp.ui.view.main.MainActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class FirebaseUtil : FirebaseMessagingService() {
    //String에 넣으면 NPE 발생. 여기서 직접 초기화해야하는 듯?
    private val CHANNEL_ID = "firebase_id"
    private val CHANNEL_DESC = "firebase_desc"

    /**
     * FCM 서버로부터 받는 데이터 타입이 data와 notification이 있음
     * https://firebase.google.com/docs/cloud-messaging/concept-options?hl=ko
     */
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.putExtra("url", remoteMessage.data["url"])
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)

        //해당 옵션은 Foreground일 때만 적용되는 옵션(icon의 경우 Backgrdound는 manifest에 정의 필요)
        val builder =
            NotificationCompat.Builder(this, CHANNEL_ID).setSmallIcon(R.drawable.ic_exclamation)
                .setContentTitle(remoteMessage.notification!!.title)
                .setContentText(remoteMessage.notification!!.body)
                .setDefaults(NotificationCompat.DEFAULT_SOUND)
                .setPriority(NotificationCompat.PRIORITY_HIGH).setAutoCancel(true)
                .setContentIntent(pendingIntent)
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager

        //오레오 버전 이후에선 채널을 꼭 붙여줘야함
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name: CharSequence = CHANNEL_ID
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, name, importance)
            notificationManager.createNotificationChannel(channel)
        }
        notificationManager.notify(0, builder.build())
    }

    override fun onNewToken(s: String) {
        super.onNewToken(s)
        BaseUtil().log(LogType.D, "token", s)
    }
}