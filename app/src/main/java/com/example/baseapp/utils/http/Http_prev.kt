package com.example.baseapp.utils.http

import com.example.baseapp.BuildConfig
import com.example.baseapp.utils.Constant
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object Http_prev {
    private const val serverURL = Constant.serverUrl
    private const val timeout: Long = 6

    var retrofit: Retrofit
    var httpInterface: HttpInterface

    init {
        //set log option and http connection option
        val logging = HttpLoggingInterceptor().apply {
            level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
            else HttpLoggingInterceptor.Level.NONE
        }
        val httpClient = OkHttpClient.Builder()
            .addInterceptor(logging)
            .connectTimeout(timeout, TimeUnit.SECONDS)
            .readTimeout(timeout, TimeUnit.SECONDS)
            .writeTimeout(timeout, TimeUnit.SECONDS)

        retrofit = Retrofit.Builder()
            .baseUrl(serverURL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build()

        httpInterface = retrofit.create(HttpInterface::class.java)
    }

    fun <T> call(call: Call<T>, listener: OnResultListener<T>) {
        call.enqueue(object : Callback<T> {
            override fun onResponse(call: Call<T>, response: Response<T>) {
                if (response.isSuccessful) {
                    listener.onResult(true, response)
                } else {
                    listener.onResult(false, null)
                }
            }

            override fun onFailure(call: Call<T>, t: Throwable) {
                listener.onResult(false, null)
            }
        })
    }

    fun interface OnResultListener<T> {
        fun onResult(isResult: Boolean, response: Response<T>?)
    }
}