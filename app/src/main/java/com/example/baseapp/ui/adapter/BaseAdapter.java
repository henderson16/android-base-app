//package com.example.baseapp.ui.adapter;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.TextView;
//
//import androidx.annotation.NonNull;
//import androidx.recyclerview.widget.RecyclerView;
//
//import com.example.baseapp.R;
//
//import java.util.ArrayList;
//
//public class BaseAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
//	private Context context;
//	private ArrayList<String> list = new ArrayList();
//
//	public BaseAdapter(Context context) {
//		this.context = context;
//	}
//
//	@NonNull
//	@Override
//	public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//		View view = inflater.inflate(R.layout.item_base, parent, false);
//		return new Cell(view);
//	}
//
//	@Override
//	public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
//		Cell cell = (Cell)holder;
//		String data = list.get(position);
//
//		cell.title.setText(data);
//	}
//
//	@Override
//	public int getItemCount() {
//		return list.size();
//	}
//
//	public void setItem(ArrayList<String> data) {
//		list = data;
//		notifyDataSetChanged();
//	}
//
//	class Cell extends RecyclerView.ViewHolder {
//		TextView title = itemView.findViewById(R.id.title);
//
//		public Cell(@NonNull View itemView) {
//			super(itemView);
//		}
//	}
//}
