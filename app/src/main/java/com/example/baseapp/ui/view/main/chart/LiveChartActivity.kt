package com.example.baseapp.ui.view.main.chart

import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import com.example.baseapp.R
import com.example.baseapp.databinding.ActivityLiveChartBinding
import com.example.baseapp.ui.view.base.BaseActivity
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import java.util.*

class LiveChartActivity : BaseActivity<ActivityLiveChartBinding>() {
    override val layoutResourceID: Int
        get() = R.layout.activity_live_chart

    private val yAxisMinRange = 0
    private val yAxisMaxRange = 10

    lateinit var addingData: Handler


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initView()
        initChart()

        addingData = Handler(Looper.getMainLooper())
        addingData.postDelayed(object : Runnable {
            override fun run() {
                addEntry(Random().nextInt(yAxisMaxRange).toFloat())
                addingData.postDelayed(this, 100)
            }
        }, 100)
    }

    override fun onPause() {
        super.onPause()
        addingData.removeCallbacksAndMessages(null)
    }

    private fun initView() {
        binding.activity = this
    }

    private fun initChart() {
        binding.chart.data = LineData()

        binding.chart.setDrawGridBackground(true)
        binding.chart.setBackgroundColor(Color.BLACK)
        binding.chart.setGridBackgroundColor(Color.BLACK)

        binding.chart.isDragEnabled = false
        binding.chart.setTouchEnabled(false)
        binding.chart.setScaleEnabled(false)
        binding.chart.setPinchZoom(false)

        binding.chart.xAxis.isEnabled = false

        binding.chart.axisLeft.isEnabled = true
        binding.chart.axisLeft.textColor = Color.BLUE
        binding.chart.axisLeft.setDrawGridLines(true)
        binding.chart.axisLeft.gridColor = Color.GREEN
        binding.chart.axisLeft.axisMaximum = yAxisMaxRange.toFloat()
        binding.chart.axisLeft.axisMinimum = yAxisMinRange.toFloat()
        binding.chart.axisRight.isEnabled = false

        val l: Legend = binding.chart.legend
        l.isEnabled = true
        l.formSize = 10f

        l.textSize = 12f
        l.textColor = Color.WHITE

        binding.chart.data.addDataSet(createSet())
        binding.chart.invalidate()
    }

    private fun addEntry(num: Float) {
        val data = binding.chart.data
        val set = data.getDataSetByIndex(0)
        val entry = Entry(set.entryCount.toFloat(), num)
        data.addEntry(entry, 0)
        data.notifyDataChanged()

        binding.chart.notifyDataSetChanged()
        binding.chart.setVisibleXRangeMaximum(yAxisMaxRange.toFloat() * 2)
        binding.chart.moveViewTo(data.entryCount.toFloat(), 50f, YAxis.AxisDependency.LEFT)
    }

    private fun createSet(): LineDataSet {
        val values = ArrayList<Entry>()
        for (i in 0 until 10) {
            val entry = Entry(i.toFloat(), 0F)
            values.add(entry)
        }
        val set = LineDataSet(values, "Real-time Line Data")
        set.lineWidth = 1f
        set.setDrawValues(false)
        set.valueTextColor = Color.WHITE
        set.color = Color.WHITE
        set.mode = LineDataSet.Mode.LINEAR
        set.setDrawCircles(false)
        set.highLightColor = Color.rgb(190, 190, 190)
        set.mode = LineDataSet.Mode.CUBIC_BEZIER
        set.cubicIntensity = 0.2F
        return set
    }

    fun clickButton(view: View) {
        addEntry(Random().nextInt(yAxisMaxRange).toFloat())
    }
}