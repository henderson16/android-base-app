package com.example.baseapp.ui.view.main

import android.Manifest
import android.content.Intent
import android.graphics.drawable.GradientDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.example.baseapp.R
import com.example.baseapp.databinding.ActivityMainBinding
import com.example.baseapp.ui.adapter.TipAdapter
import com.example.baseapp.ui.dialog.BaseModalBottomSheet
import com.example.baseapp.ui.dialog.ScrollModalBottomSheet
import com.example.baseapp.ui.view.base.NavigationActivity
import com.example.baseapp.ui.view.main.ble.BleScanActivity
import com.example.baseapp.ui.view.main.ble.BluetoothScanActivity
import com.example.baseapp.ui.view.main.camera.CameraActivity
import com.example.baseapp.ui.view.main.chart.LiveChartActivity
import com.example.baseapp.ui.view.main.foot.FooterAnimationActivity
import com.example.baseapp.ui.view.main.gps.ForegroundLocationServiceActivity
import com.example.baseapp.ui.view.web.RecyclerViewActivity
import com.example.baseapp.ui.view.web.WebViewActivity
import com.example.baseapp.utils.BaseUtil
import com.example.baseapp.utils.LogType
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.android.material.bottomsheet.BottomSheetBehavior
import java.util.*


class MainActivity : NavigationActivity<ActivityMainBinding>() {
    override val layoutResourceID: Int
        get() = R.layout.activity_main

    //Local value for Google Sign-in
    private val googleSignInResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { activityResult ->
            val task: Task<GoogleSignInAccount> =
                GoogleSignIn.getSignedInAccountFromIntent(activityResult.data)
            handleSignInResult(task)
        }
    private lateinit var googleSignInClient: GoogleSignInClient

    //Local Variable, Constants for ViewPagerIndicator
    private var tipAdapter: TipAdapter? = null
    private var tipIndicator: ArrayList<ImageView?>? = null
    private val bannerInterval: Long = 5000
    private val indicatorMargin = 6

    // 핸들러를 사용할 땐 해제도 항상 고려해야됨(핸들러는 해제하지 않으면 다른 화면, 백그라운드에서도 유지되기 때문에 onResume, onPause에서 제어)
    private val handler = Handler()
    private val runnableForBanner: Runnable = object : Runnable {
        override fun run() {
            goToNextBanner()
            handler.postDelayed(this, bannerInterval)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //Check fcm data
        if (intent.extras != null) {
            for (key in intent.extras!!.keySet()) {
                val value = intent.extras!!.getString(key)
                BaseUtil().log(LogType.D, "message", value!!)
            }
        }

        setGoogleLoginSetting()
        initView()
        initToolBar(binding.toolbar)
    }

    override fun onResume() {
        super.onResume()
        handler.postDelayed(runnableForBanner, bannerInterval)
    }

    override fun onPause() {
        super.onPause()
        handler.removeCallbacksAndMessages(null)
    }

    private fun setGoogleLoginSetting() {
        val gso =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build()

        googleSignInClient = GoogleSignIn.getClient(this, gso)
    }

    private fun initView() {
        //Set event to Navigation
        super.initNavigation(binding.drawer)

        binding.activity = this
        binding.hamburger.setOnClickListener { clickHamburger() }

        //Init ViewPager
        tipAdapter = TipAdapter()
        binding.investTipBanner.adapter = tipAdapter
        binding.investTipBanner.registerOnPageChangeCallback(object : OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                changeBannerIndicator()
            }
        })

        //Init ViewPagerIndicator
        initBannerIndicator()
    }

    private fun goToNextBanner() {
        var currentCount = binding.investTipBanner.currentItem
        if (currentCount < tipAdapter!!.bannerCount - 1) {
            currentCount++
        } else {
            currentCount = 0
        }
        binding.investTipBanner.currentItem = currentCount
        changeBannerIndicator()
    }

    private fun initBannerIndicator() {
        tipIndicator = ArrayList()
        for (i in 0 until tipAdapter!!.bannerCount) {
            val image = ImageView(this)
            binding.investTipBannerIndicator.addView(image)
            tipIndicator?.add(image)
            image.setImageResource(R.drawable.ic_circle)
            val params = image.layoutParams as LinearLayout.LayoutParams
            params.setMargins(indicatorMargin, 0, indicatorMargin, 0)
            image.layoutParams = params
        }
        changeBannerIndicator()
    }

    private fun changeBannerIndicator() {
        for (i in tipIndicator!!.indices) {
            val drawable = tipIndicator!![i]!!.drawable as GradientDrawable
            if (i == binding.investTipBanner.currentItem) {
                drawable.setColor(getColor(R.color.black))
            } else {
                drawable.setColor(getColor(R.color.gray))
            }
        }
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            BaseUtil().log(LogType.D, "handleSignInResult", "$account.account")
        } catch (e: ApiException) {
            BaseUtil().log(LogType.E, "handleSignInResult", "$e")
        }
    }

    fun clickGps(view: View) {
        startActivity(Intent(this, ForegroundLocationServiceActivity::class.java))
    }

    fun clickLiveChart(view: View) {
        startActivity(Intent(this, LiveChartActivity::class.java))
    }

    fun goToFootAnimationActivity(view: View) {
        val intent = Intent(this, FooterAnimationActivity::class.java)
        startActivity(intent)
    }

    fun clickWebView(view: View) {
        startActivity(Intent(this, WebViewActivity::class.java))
    }

    fun clickRecyclerView(view: View) {
        startActivity(Intent(this, RecyclerViewActivity::class.java))
    }

    fun clickGoogleLogin(view: View) {
        val intent = googleSignInClient.signInIntent
        googleSignInResult.launch(intent)
    }

    fun clickAlarm(view: View) {
        BaseUtil().playSound(this)
        BaseUtil().playVibration(this)
    }

    fun clickBle(view: View) {
        startActivity(Intent(this, BleScanActivity::class.java))
    }

    fun clickBluetooth(view: View) {
        startActivity(Intent(this, BluetoothScanActivity::class.java))
    }

    private val photoActivityResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == RESULT_OK) {
            if (result.data != null) {
                val uri = Uri.parse(result.data?.extras?.getString("imageUri"))
//                cropImage(uri)
            }
        }
    }

    fun clickCamera(view: View) {
        runWithPermission(Manifest.permission.CAMERA) {
            photoActivityResult.launch(Intent(this, CameraActivity::class.java))
        }
    }

    fun showModalBottomSheet(view: View) {
        val dialog = BaseModalBottomSheet(this)
        dialog.show(supportFragmentManager, "")
    }

    fun showPersistentBottomSheet(view: View) {
        val bottomSheet = BottomSheetBehavior.from(binding.persistentBottomSheet.body)
        bottomSheet.state = BottomSheetBehavior.STATE_EXPANDED
    }

    fun showScrollModalBottomSheet(view: View) {
        val dialog = ScrollModalBottomSheet(this)
        dialog.show(supportFragmentManager, "")
    }
    /* */
}