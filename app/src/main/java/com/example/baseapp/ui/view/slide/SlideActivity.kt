package com.example.baseapp.ui.view.slide

import android.os.Bundle
import androidx.activity.viewModels
import com.example.baseapp.R
import com.example.baseapp.databinding.ActivitySlideBinding
import com.example.baseapp.ui.view.base.BaseActivity

class SlideActivity: BaseActivity<ActivitySlideBinding>() {
    override val layoutResourceID: Int
        get() = R.layout.activity_slide

    private val viewModel: SlideViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    private fun initView() {
    }
}