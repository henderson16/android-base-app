package com.example.baseapp.ui.view.base

import android.content.Intent
import android.view.View
import androidx.core.view.GravityCompat
import androidx.databinding.ViewDataBinding
import androidx.drawerlayout.widget.DrawerLayout
import com.example.baseapp.R
import com.example.baseapp.ui.view.slide.SlideActivity

abstract class NavigationActivity<T : ViewDataBinding> : BaseActivity<T>(){
    var drawer: DrawerLayout? = null

    //Set event to Navigation
    open fun initNavigation(drawer: DrawerLayout) {
        this.drawer = drawer

        drawer.findViewById<View>(R.id.navigation_proportion).setOnClickListener {
            drawer.closeDrawer(GravityCompat.END)
            startActivity(Intent(this, SlideActivity::class.java))
        }
    }

    protected fun clickHamburger() {
        if (drawer!!.isDrawerOpen(GravityCompat.END)) {
            drawer?.closeDrawer(GravityCompat.END)
        } else {
            drawer?.openDrawer(GravityCompat.END)
        }
    }

    override fun onBackPressed() {
        if (drawer!!.isDrawerOpen(GravityCompat.END)) {
            drawer!!.closeDrawer(GravityCompat.END)
        } else {
            super.onBackPressed()
        }
    }
}