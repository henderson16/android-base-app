package com.example.baseapp.ui.view.base

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.MenuItem
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.example.baseapp.ui.dialog.LoadingDialog
import com.example.baseapp.utils.BaseUtil
import com.example.baseapp.utils.LogType

enum class PermissionType {
    GPS, CAMERA, STORAGE
}

enum class RequestCodeType(val value: Int) {
    GOOGLE_SIGN_IN(1), CAMERA(2), GALLERY(3);
}

abstract class BaseActivity<T : ViewDataBinding> : AppCompatActivity() {
    lateinit var binding: T
    abstract val layoutResourceID: Int

    private var loadingDialog: LoadingDialog? = null

    private var permissionCallback: (() -> Unit)? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        BaseUtil().log(LogType.D, "onCreate")

        initBinding()
    }

    override fun onStart() {
        super.onStart()
        BaseUtil().log(LogType.D, "onCreate")
    }

    override fun onResume() {
        super.onResume()
        BaseUtil().log(LogType.D, "onResume")
    }

    override fun onPause() {
        super.onPause()
        BaseUtil().log(LogType.D, "onPause")
    }

    override fun onStop() {
        super.onStop()
        BaseUtil().log(LogType.D, "onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        BaseUtil().log(LogType.D, "onDestroy")
    }

    private fun initBinding() {
        binding = DataBindingUtil.setContentView(this, layoutResourceID)
        binding.lifecycleOwner = this
    }

    protected fun showLoadingDialog() {
        if (loadingDialog == null) {
            loadingDialog = LoadingDialog(this)
        }
        loadingDialog!!.show()
    }

    protected fun hideLoadingDialog() {
        if (loadingDialog!!.isShowing) {
            loadingDialog!!.dismiss()
        }
    }

    protected fun initToolBar(toolbar: Toolbar) {
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayShowTitleEnabled(false)

        if (isTaskRoot) {
            supportActionBar?.setDisplayShowHomeEnabled(false)
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
        } else {
            supportActionBar?.setDisplayShowHomeEnabled(true)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                //toolbar의 back키 눌렀을 때 동작
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    /**
     * 권한 처리(이벤트 전달 방식)
     */
    private val locationPermissionResult =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted: Boolean ->
            if (isGranted) {
//                hasPermission(true)
            } else {
//                hasPermission(false)
            }
        }

    //Test at WebViewActivity
    fun check() {
        checkPermission(PermissionType.GPS, locationPermissionResult)?.let {
            if (it) {
//                hasPermission(true)
            } else {
//                hasPermission(false)
            }
        }
    }

    fun checkPermission(type: PermissionType, callback: ActivityResultLauncher<String>): Boolean? {
        val permission = when (type) {
            PermissionType.GPS -> Manifest.permission.ACCESS_FINE_LOCATION
            PermissionType.CAMERA -> Manifest.permission.CAMERA
            PermissionType.STORAGE -> Manifest.permission.READ_EXTERNAL_STORAGE
        }
        if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                callback.launch(permission)
                return null
            } else {
                callback.launch(permission)
                return null
            }
        } else {
            return true
        }
    }

    /* 새로운 퍼미션 체크 방법 */
    fun runWithPermission(permission: String, callback: (() -> Unit)) {
        permissionCallback = callback
        permissionActivityResult.launch(permission)
    }

    private val permissionActivityResult = registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
        if (isGranted) {
            permissionCallback?.let { it() }
        } else {
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            val uri = Uri.fromParts("package", packageName, null)
            intent.data = uri
            startActivity(intent)
        }
        permissionCallback = null
    }
}
