package com.example.baseapp.ui.dialog

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.baseapp.R
import com.example.baseapp.databinding.DialogModalBottomSheetBinding
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class BaseModalBottomSheet(internal val context: Context) : BottomSheetDialogFragment() {
    private var binding: DialogModalBottomSheetBinding? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        initView()
        return binding?.root
    }

    private fun initView() {
        val lifecycleOwner = context as AppCompatActivity

        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_modal_bottom_sheet, null, false)
        binding?.viewModel = this
        binding?.lifecycleOwner = this

        binding?.root?.setOnClickListener { hideKeyboard() }
    }

    fun clickDone(view: View) {
        dismiss()
    }

    //Set full size dialog
    private fun expandDialog() {
        val bottomSheet = dialog as BottomSheetDialog
        bottomSheet.behavior.state = BottomSheetBehavior.STATE_EXPANDED
    }

    private fun hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(dialog?.currentFocus?.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }

    override fun getTheme(): Int {
        return R.style.BottomSheetDialogWithBackground
    }
}