package com.example.baseapp.ui.dialog

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.baseapp.R
import com.example.baseapp.ui.adapter.BaseAdapter
import com.example.baseapp.databinding.DialogScrollModalBottomSheetBinding
import com.example.baseapp.utils.BaseUtil
import com.example.baseapp.utils.LogType
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import java.util.*

class ScrollModalBottomSheet(internal val context: Context) : BottomSheetDialogFragment() {
    private var binding: DialogScrollModalBottomSheetBinding? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        initView()
        setRecyclerView()
        return binding?.root
    }

    private fun initView() {
        val lifecycleOwner = context as AppCompatActivity

        binding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.dialog_scroll_modal_bottom_sheet,
            null,
            false
        )
        binding?.viewModel = this
        binding?.lifecycleOwner = this

        binding?.root?.setOnClickListener { hideKeyboard() }
    }

    private fun setRecyclerView() {
        val adapter = BaseAdapter(context)
        binding?.recyclerView?.adapter = adapter
        val aa: ArrayList<String> = ArrayList()
        aa.add("1")
        aa.add("1")
        aa.add("1")
        aa.add("1")
        aa.add("1")
        aa.add("1")
        aa.add("1")
        aa.add("1")
        aa.add("1")
        aa.add("1")
        aa.add("1")
        aa.add("1")
        aa.add("1")
        adapter.setItem(aa)

        binding?.recyclerView?.addOnScrollListener(CustomScrollListener())
    }

    fun clickDone(view: View) {
        dismiss()
    }

    private fun hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(
            dialog?.currentFocus?.windowToken,
            InputMethodManager.HIDE_NOT_ALWAYS
        )
    }

    override fun getTheme(): Int {
        return R.style.BottomSheetDialogWithBackground
    }

    inner class CustomScrollListener : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            val bottomSheet = dialog as BottomSheetDialog

            when (newState) {
                RecyclerView.SCROLL_STATE_IDLE -> {
                    BaseUtil().log(
                        LogType.D,
                        "onScrollStateChanged",
                        "The RecyclerView is not scrolling"
                    )
                    bottomSheet.behavior.isHideable = true
                }
                RecyclerView.SCROLL_STATE_DRAGGING -> {
                    BaseUtil().log(LogType.D, "onScrollStateChanged", "Scrolling now")
                    bottomSheet.behavior.isHideable = false
                }
                RecyclerView.SCROLL_STATE_SETTLING -> BaseUtil().log(
                    LogType.D,
                    "onScrollStateChanged",
                    "Scroll Settling"
                )
            }
        }

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            if (dx > 0) {
                BaseUtil().log(LogType.D, "onScrolled", "Scrolled Right")
            } else if (dx < 0) {
                BaseUtil().log(LogType.D, "onScrolled", "Scrolled Left")
            } else {
                BaseUtil().log(LogType.D, "onScrolled", "No Horizontal Scrolled")
            }
            if (dy > 0) {
                BaseUtil().log(LogType.D, "onScrolled", "Scrolled Downwards")
            } else if (dy < 0) {
                BaseUtil().log(LogType.D, "onScrolled", "Scrolled Upwards")
            } else {
                BaseUtil().log(LogType.D, "onScrolled", "No Vertical Scrolled")
            }
        }
    }
}