package com.example.baseapp.utils.http

import com.example.baseapp.model.User
import retrofit2.Response
import retrofit2.http.GET

/* 가이드 코드
val body = HashMap<String, Any>()
body["param1"] = "1"
body["param2"] = 2
val body = HashMap<String, Any>()
body["shopCode"] = "testtest1m"

val request = Http.httpInterface.checkShopCode(body)
Http.call(request) { isResult, response ->
    listener?.hideLoadingBar()
    if (!isResult) {
        listener?.showDialog(null)
        return@call
    } else {
        if (response?.body()?.resultCode != "success") {
            listener?.showDialog(response?.body()!!.resultMsg)
            return@call
        } else {
            listener?.goToMain()
        }
    }
}
* */
//interface HttpInterface {
//    @GET("/posts/1")
//    fun getData(@Body body: HashMap<String, Any>): Call<User>
//}


interface HttpInterface {
    @GET("/posts/1")
    suspend fun getData(): Response<User>
}