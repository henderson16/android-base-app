package com.example.baseapp.utils

import java.util.*

object Constant {
    const val serverUrl = "https://jsonplaceholder.typicode.com/"

    const val commonLocation = "Location"

    val SERVER_NOT_RESPONSE = "서버가 응답하지 않습니다"
    val REQUEST_APPROVAL = "request_approval"
    val GOODS_LIST = "goods_list"
    val HASH_MAP = "hash_map"

    val BLUETOOTH_UUID = UUID.fromString("fa87c0d0-afac-11de-8a39-0800200c9a66")
}