package com.example.baseapp.utils

import android.location.Location
import androidx.lifecycle.MutableLiveData

fun Location?.toText(): String {
    return if (this != null) {
        "($latitude, $longitude)"
    } else {
        "Unknown location"
    }
}

fun <T> MutableLiveData<T>.notifyObserver() {
    this.value = this.value
}