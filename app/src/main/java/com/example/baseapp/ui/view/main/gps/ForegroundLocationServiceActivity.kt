package com.example.baseapp.ui.view.main.gps

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.Location
import android.os.Build
import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.baseapp.R
import com.example.baseapp.databinding.ActivityForegroundServiceGpsBinding
import com.example.baseapp.ui.view.base.BaseActivity
import com.example.baseapp.ui.view.base.PermissionType
import com.example.baseapp.ui.dialog.PopupDialog
import com.example.baseapp.utils.Constant.commonLocation
import com.example.baseapp.utils.location.FusedLocationService
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions

class ForegroundLocationServiceActivity : BaseActivity<ActivityForegroundServiceGpsBinding>(),
    ForegroundLocationServiceViewModel.Listener, OnMapReadyCallback {
    override val layoutResourceID: Int
        get() = R.layout.activity_foreground_service_gps

    private val viewModel: ForegroundLocationServiceViewModel by viewModels()

    private lateinit var locationBroadcastReceiver: LocationBroadcastReceiver

    private lateinit var googleMap: GoogleMap
    private var m1: Marker? = null
    private var m2: Marker? = null
    private var m3: Marker? = null

    private val gpsPermissionResult =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted: Boolean ->
            if (isGranted) {
                hasPermission(true)
            } else {
                hasPermission(false)
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initView()
        initMapView()

        locationBroadcastReceiver = LocationBroadcastReceiver()

        FusedLocationService.isRunning.observe(this, {
            if (it) {
                binding.startFusedLocation.text = "Stop Service"
            } else {
                binding.startFusedLocation.text = "Start Service"
            }
        })
    }

    override fun onResume() {
        super.onResume()

        LocalBroadcastManager.getInstance(this).registerReceiver(
            locationBroadcastReceiver, IntentFilter(
                FusedLocationService().action
            )
        )

        LocalBroadcastManager.getInstance(this).registerReceiver(
            locationBroadcastReceiver, IntentFilter(
//                GpsLocationService().action
            )
        )

        LocalBroadcastManager.getInstance(this).registerReceiver(
            locationBroadcastReceiver, IntentFilter(
//                NetworkLocationService().action
            )
        )
    }

    override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(
            locationBroadcastReceiver
        )

        super.onPause()
    }

    private fun initView() {
        binding.viewModel = viewModel
        viewModel.listener = this
    }

    private fun initMapView() {
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)
    }

    private fun hasPermission(has: Boolean) {
        if (has) {
            if (FusedLocationService.isRunning.value == false) {
                val intent = Intent(this, FusedLocationService::class.java)
                intent.action = "START"
                startService(intent)
            } else {
                val intent = Intent(this, FusedLocationService::class.java)
                intent.action = "STOP"
                startService(intent)
            }
        } else {
            PopupDialog(this, "권한 없음").show()
        }
    }

    /* Listener */
    override fun gpsForegroundService() {
        checkPermission(PermissionType.GPS, gpsPermissionResult)?.let {
            if (it) {
                hasPermission(true)
            } else {
                hasPermission(false)
            }
        }
    }

    override fun startGpsService() {
//        val intent = Intent(this, GpsLocationService::class.java)
//        intent.action = "START"
//        startService(intent)
    }

    override fun startNetworkService() {
//        val intent = Intent(this, NetworkLocationService::class.java)
//        intent.action = "START"
//        startService(intent)
    }
    /**/

    /* OnMapReadyCallback */
    override fun onMapReady(googleMap: GoogleMap) {
        this.googleMap = googleMap

        val latLng = LatLng(37.3386853, 127.1019836)
        val cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 17F)
        googleMap.moveCamera(cameraUpdate)
    }
    /**/

    private inner class LocationBroadcastReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
//            val location = intent.getParcelableExtra(commonLocation) as Location
//            val latLng = LatLng(location.latitude, location.longitude)
//
//            //TODO: change to newer way
//            val location = if (Build.VERSION.SDK_INT >= 33) {
//                intent.getParcelableExtra(commonLocation, Location::class.java)
//            } else {
//                intent.getParcelableExtra(commonLocation)
//            }
//            val latLng = LatLng(location!!.latitude, location.longitude)
//
//            when (intent.action) {
//                FusedLocationService().action -> {
//                    m1?.remove()
//                    val marker = MarkerOptions().position(latLng).title("fused").icon(
//                        BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)
//                    )
//                    m1 = googleMap.addMarker(marker)
//                }
//                GpsLocationService().action -> {
//                    m2?.remove()
//                    val marker = MarkerOptions().position(latLng).title("gps").icon(
//                        BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)
//                    )
//                    m2 = googleMap.addMarker(marker)
//                }
//                NetworkLocationService().action -> {
//                    m3?.remove()
//                    val marker = MarkerOptions().position(latLng).title("network").icon(
//                        BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)
//                    )
//                    m3 = googleMap.addMarker(marker)
//                }
//                else -> {
//                    null
//                }
//            }
        }
    }
}